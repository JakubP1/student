Rails.application.routes.draw do
  resources :grades
  resources :subjects
  resources :students
  
  root "pages#show", page: "index"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
