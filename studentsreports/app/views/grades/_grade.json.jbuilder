json.extract! grade, :id, :value, :date, :student_id, :subject_id, :created_at, :updated_at
json.url grade_url(grade, format: :json)
